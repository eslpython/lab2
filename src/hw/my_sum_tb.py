from myhdl import block, instance, Signal, ResetSignal, StopSimulation, instances, delay
import os
from myhdl import block, always_seq, Signal, intbv, enum, instances


@block
def my_sum(clk, reset, axis_s_raw, axis_m_sum):
    state_t = enum('COUNT', 'WRITE_COUNT', 'WRITE_SUM')
    accumulator = Signal(intbv(0)[32:])
    counter = Signal(intbv(0)[32:])
    state = Signal(state_t.COUNT)

    @always_seq(clk.posedge, reset=reset)
    def sum_proc():
        if state == state_t.COUNT:
            axis_m_sum.tvalid.next = 0
            axis_m_sum.tlast.next = 0
            axis_s_raw.tready.next = 1
            if axis_s_raw.tvalid == 1:
                accumulator.next = accumulator + axis_s_raw.tdata
                counter.next = counter + 1
                if axis_s_raw.tlast == 1:
                    state.next = state_t.WRITE_COUNT
        elif state == state_t.WRITE_COUNT:
            axis_s_raw.tready.next = 0
            axis_m_sum.tvalid.next = 1
            axis_m_sum.tlast.next = 0
            axis_m_sum.tdata.next = counter
            if axis_m_sum.tready == 1:
                state.next = state_t.WRITE_SUM
                counter.next = 0
        elif state == state_t.WRITE_SUM:
            axis_s_raw.tready.next = 0
            axis_m_sum.tvalid.next = 1
            axis_m_sum.tlast.next = 1
            axis_m_sum.tdata.next = accumulator
            if axis_m_sum.tready == 1:
                state.next = state_t.COUNT
                accumulator.next = 0
        else:
            raise ValueError("Undefined state")

    return instances()
# from axis import *
from clk_stim import clk_stim


from myhdl import Signal, modbv


class Axis:
    def __init__(self, data_width):
        self.tdata = Signal(modbv(0)[data_width:])
        self.tready = Signal(bool(0))
        self.tvalid = Signal(bool(0))
        self.tlast = Signal(bool(0))


@block
def testbench(vhdl_output_path=None):

    reset = ResetSignal(0, active=0, async=False)
    clk = Signal(bool(0))

    axis_raw = Axis(32)
    axis_sum = Axis(32)

    clk_gen = clk_stim(clk, period=10)

    @instance
    def reset_gen():
        reset.next = 0
        yield delay(54)
        yield clk.negedge
        reset.next = 1

    @instance
    def write_stim():
        values = list(range(50, 100))
        i = 0
        yield reset.posedge
        while i < len(values):
            yield clk.negedge
            axis_raw.tvalid.next = 1
            axis_raw.tdata.next = values[i]
            if i == len(values) - 1:
                axis_raw.tlast.next = 1
            else:
                axis_raw.tlast.next = 0
            if axis_raw.tready == 1:
                i += 1
        yield clk.negedge
        axis_raw.tvalid.next = 0

    @instance
    def read_stim():
        yield reset.posedge
        yield delay(601)
        yield clk.negedge
        axis_sum.tready.next = 1
        while True:
            yield clk.negedge
            if axis_sum.tlast == 1:
                break

        for i in range(10):
            yield clk.negedge
        raise StopSimulation()

    uut = my_sum(clk, reset, axis_raw, axis_sum)

    if vhdl_output_path is not None:
        uut.convert(hdl='VHDL', path=vhdl_output_path)
    return instances()


if __name__ == '__main__':
    trace_save_path = '../out/testbench/'
    vhdl_output_path = '../out/vhdl/'
    os.makedirs(os.path.dirname(trace_save_path), exist_ok=True)
    os.makedirs(os.path.dirname(vhdl_output_path), exist_ok=True)

    tb = testbench(vhdl_output_path)
    tb.config_sim(trace=True, directory=trace_save_path, name='my_sum_tb')
    tb.run_sim()
