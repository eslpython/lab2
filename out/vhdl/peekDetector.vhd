-- File: out/vhdl/peekDetector.vhd
-- Generated by MyHDL 0.10
-- Date: Mon Dec 10 19:42:28 2018


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use std.textio.all;

use work.pck_myhdl_010.all;

entity peekDetector is
    port (
        clk: in std_logic;
        reset: in std_logic;
        inputSignal: inout unsigned(3199 downto 0);
        outputSignal: out unsigned(3199 downto 0)
    );
end entity peekDetector;


architecture MyHDL of peekDetector is


signal out_c: unsigned(3199 downto 0);
type t_array_outputSignal2 is array(0 to 100-1) of unsigned(31 downto 0);
signal outputSignal2: t_array_outputSignal2;
type t_array_input_sliced is array(0 to 100-1) of unsigned(31 downto 0);
signal input_sliced: t_array_input_sliced;

begin



out_c(3200-1 downto 3168) <= outputSignal2(0);
out_c(3168-1 downto 3136) <= outputSignal2(1);
out_c(3136-1 downto 3104) <= outputSignal2(2);
out_c(3104-1 downto 3072) <= outputSignal2(3);
out_c(3072-1 downto 3040) <= outputSignal2(4);
out_c(3040-1 downto 3008) <= outputSignal2(5);
out_c(3008-1 downto 2976) <= outputSignal2(6);
out_c(2976-1 downto 2944) <= outputSignal2(7);
out_c(2944-1 downto 2912) <= outputSignal2(8);
out_c(2912-1 downto 2880) <= outputSignal2(9);
out_c(2880-1 downto 2848) <= outputSignal2(10);
out_c(2848-1 downto 2816) <= outputSignal2(11);
out_c(2816-1 downto 2784) <= outputSignal2(12);
out_c(2784-1 downto 2752) <= outputSignal2(13);
out_c(2752-1 downto 2720) <= outputSignal2(14);
out_c(2720-1 downto 2688) <= outputSignal2(15);
out_c(2688-1 downto 2656) <= outputSignal2(16);
out_c(2656-1 downto 2624) <= outputSignal2(17);
out_c(2624-1 downto 2592) <= outputSignal2(18);
out_c(2592-1 downto 2560) <= outputSignal2(19);
out_c(2560-1 downto 2528) <= outputSignal2(20);
out_c(2528-1 downto 2496) <= outputSignal2(21);
out_c(2496-1 downto 2464) <= outputSignal2(22);
out_c(2464-1 downto 2432) <= outputSignal2(23);
out_c(2432-1 downto 2400) <= outputSignal2(24);
out_c(2400-1 downto 2368) <= outputSignal2(25);
out_c(2368-1 downto 2336) <= outputSignal2(26);
out_c(2336-1 downto 2304) <= outputSignal2(27);
out_c(2304-1 downto 2272) <= outputSignal2(28);
out_c(2272-1 downto 2240) <= outputSignal2(29);
out_c(2240-1 downto 2208) <= outputSignal2(30);
out_c(2208-1 downto 2176) <= outputSignal2(31);
out_c(2176-1 downto 2144) <= outputSignal2(32);
out_c(2144-1 downto 2112) <= outputSignal2(33);
out_c(2112-1 downto 2080) <= outputSignal2(34);
out_c(2080-1 downto 2048) <= outputSignal2(35);
out_c(2048-1 downto 2016) <= outputSignal2(36);
out_c(2016-1 downto 1984) <= outputSignal2(37);
out_c(1984-1 downto 1952) <= outputSignal2(38);
out_c(1952-1 downto 1920) <= outputSignal2(39);
out_c(1920-1 downto 1888) <= outputSignal2(40);
out_c(1888-1 downto 1856) <= outputSignal2(41);
out_c(1856-1 downto 1824) <= outputSignal2(42);
out_c(1824-1 downto 1792) <= outputSignal2(43);
out_c(1792-1 downto 1760) <= outputSignal2(44);
out_c(1760-1 downto 1728) <= outputSignal2(45);
out_c(1728-1 downto 1696) <= outputSignal2(46);
out_c(1696-1 downto 1664) <= outputSignal2(47);
out_c(1664-1 downto 1632) <= outputSignal2(48);
out_c(1632-1 downto 1600) <= outputSignal2(49);
out_c(1600-1 downto 1568) <= outputSignal2(50);
out_c(1568-1 downto 1536) <= outputSignal2(51);
out_c(1536-1 downto 1504) <= outputSignal2(52);
out_c(1504-1 downto 1472) <= outputSignal2(53);
out_c(1472-1 downto 1440) <= outputSignal2(54);
out_c(1440-1 downto 1408) <= outputSignal2(55);
out_c(1408-1 downto 1376) <= outputSignal2(56);
out_c(1376-1 downto 1344) <= outputSignal2(57);
out_c(1344-1 downto 1312) <= outputSignal2(58);
out_c(1312-1 downto 1280) <= outputSignal2(59);
out_c(1280-1 downto 1248) <= outputSignal2(60);
out_c(1248-1 downto 1216) <= outputSignal2(61);
out_c(1216-1 downto 1184) <= outputSignal2(62);
out_c(1184-1 downto 1152) <= outputSignal2(63);
out_c(1152-1 downto 1120) <= outputSignal2(64);
out_c(1120-1 downto 1088) <= outputSignal2(65);
out_c(1088-1 downto 1056) <= outputSignal2(66);
out_c(1056-1 downto 1024) <= outputSignal2(67);
out_c(1024-1 downto 992) <= outputSignal2(68);
out_c(992-1 downto 960) <= outputSignal2(69);
out_c(960-1 downto 928) <= outputSignal2(70);
out_c(928-1 downto 896) <= outputSignal2(71);
out_c(896-1 downto 864) <= outputSignal2(72);
out_c(864-1 downto 832) <= outputSignal2(73);
out_c(832-1 downto 800) <= outputSignal2(74);
out_c(800-1 downto 768) <= outputSignal2(75);
out_c(768-1 downto 736) <= outputSignal2(76);
out_c(736-1 downto 704) <= outputSignal2(77);
out_c(704-1 downto 672) <= outputSignal2(78);
out_c(672-1 downto 640) <= outputSignal2(79);
out_c(640-1 downto 608) <= outputSignal2(80);
out_c(608-1 downto 576) <= outputSignal2(81);
out_c(576-1 downto 544) <= outputSignal2(82);
out_c(544-1 downto 512) <= outputSignal2(83);
out_c(512-1 downto 480) <= outputSignal2(84);
out_c(480-1 downto 448) <= outputSignal2(85);
out_c(448-1 downto 416) <= outputSignal2(86);
out_c(416-1 downto 384) <= outputSignal2(87);
out_c(384-1 downto 352) <= outputSignal2(88);
out_c(352-1 downto 320) <= outputSignal2(89);
out_c(320-1 downto 288) <= outputSignal2(90);
out_c(288-1 downto 256) <= outputSignal2(91);
out_c(256-1 downto 224) <= outputSignal2(92);
out_c(224-1 downto 192) <= outputSignal2(93);
out_c(192-1 downto 160) <= outputSignal2(94);
out_c(160-1 downto 128) <= outputSignal2(95);
out_c(128-1 downto 96) <= outputSignal2(96);
out_c(96-1 downto 64) <= outputSignal2(97);
out_c(64-1 downto 32) <= outputSignal2(98);
out_c(32-1 downto 0) <= outputSignal2(99);
inputSignal(3200-1 downto 3168) <= None;
inputSignal(3168-1 downto 3136) <= None;
inputSignal(3136-1 downto 3104) <= None;
inputSignal(3104-1 downto 3072) <= None;
inputSignal(3072-1 downto 3040) <= None;
inputSignal(3040-1 downto 3008) <= None;
inputSignal(3008-1 downto 2976) <= None;
inputSignal(2976-1 downto 2944) <= None;
inputSignal(2944-1 downto 2912) <= None;
inputSignal(2912-1 downto 2880) <= None;
inputSignal(2880-1 downto 2848) <= None;
inputSignal(2848-1 downto 2816) <= None;
inputSignal(2816-1 downto 2784) <= None;
inputSignal(2784-1 downto 2752) <= None;
inputSignal(2752-1 downto 2720) <= None;
inputSignal(2720-1 downto 2688) <= None;
inputSignal(2688-1 downto 2656) <= None;
inputSignal(2656-1 downto 2624) <= None;
inputSignal(2624-1 downto 2592) <= None;
inputSignal(2592-1 downto 2560) <= None;
inputSignal(2560-1 downto 2528) <= None;
inputSignal(2528-1 downto 2496) <= None;
inputSignal(2496-1 downto 2464) <= None;
inputSignal(2464-1 downto 2432) <= None;
inputSignal(2432-1 downto 2400) <= None;
inputSignal(2400-1 downto 2368) <= None;
inputSignal(2368-1 downto 2336) <= None;
inputSignal(2336-1 downto 2304) <= None;
inputSignal(2304-1 downto 2272) <= None;
inputSignal(2272-1 downto 2240) <= None;
inputSignal(2240-1 downto 2208) <= None;
inputSignal(2208-1 downto 2176) <= None;
inputSignal(2176-1 downto 2144) <= None;
inputSignal(2144-1 downto 2112) <= None;
inputSignal(2112-1 downto 2080) <= None;
inputSignal(2080-1 downto 2048) <= None;
inputSignal(2048-1 downto 2016) <= None;
inputSignal(2016-1 downto 1984) <= None;
inputSignal(1984-1 downto 1952) <= None;
inputSignal(1952-1 downto 1920) <= None;
inputSignal(1920-1 downto 1888) <= None;
inputSignal(1888-1 downto 1856) <= None;
inputSignal(1856-1 downto 1824) <= None;
inputSignal(1824-1 downto 1792) <= None;
inputSignal(1792-1 downto 1760) <= None;
inputSignal(1760-1 downto 1728) <= None;
inputSignal(1728-1 downto 1696) <= None;
inputSignal(1696-1 downto 1664) <= None;
inputSignal(1664-1 downto 1632) <= None;
inputSignal(1632-1 downto 1600) <= None;
inputSignal(1600-1 downto 1568) <= None;
inputSignal(1568-1 downto 1536) <= None;
inputSignal(1536-1 downto 1504) <= None;
inputSignal(1504-1 downto 1472) <= None;
inputSignal(1472-1 downto 1440) <= None;
inputSignal(1440-1 downto 1408) <= None;
inputSignal(1408-1 downto 1376) <= None;
inputSignal(1376-1 downto 1344) <= None;
inputSignal(1344-1 downto 1312) <= None;
inputSignal(1312-1 downto 1280) <= None;
inputSignal(1280-1 downto 1248) <= None;
inputSignal(1248-1 downto 1216) <= None;
inputSignal(1216-1 downto 1184) <= None;
inputSignal(1184-1 downto 1152) <= None;
inputSignal(1152-1 downto 1120) <= None;
inputSignal(1120-1 downto 1088) <= None;
inputSignal(1088-1 downto 1056) <= None;
inputSignal(1056-1 downto 1024) <= None;
inputSignal(1024-1 downto 992) <= None;
inputSignal(992-1 downto 960) <= None;
inputSignal(960-1 downto 928) <= None;
inputSignal(928-1 downto 896) <= None;
inputSignal(896-1 downto 864) <= None;
inputSignal(864-1 downto 832) <= None;
inputSignal(832-1 downto 800) <= None;
inputSignal(800-1 downto 768) <= None;
inputSignal(768-1 downto 736) <= None;
inputSignal(736-1 downto 704) <= None;
inputSignal(704-1 downto 672) <= None;
inputSignal(672-1 downto 640) <= None;
inputSignal(640-1 downto 608) <= None;
inputSignal(608-1 downto 576) <= None;
inputSignal(576-1 downto 544) <= None;
inputSignal(544-1 downto 512) <= None;
inputSignal(512-1 downto 480) <= None;
inputSignal(480-1 downto 448) <= None;
inputSignal(448-1 downto 416) <= None;
inputSignal(416-1 downto 384) <= None;
inputSignal(384-1 downto 352) <= None;
inputSignal(352-1 downto 320) <= None;
inputSignal(320-1 downto 288) <= None;
inputSignal(288-1 downto 256) <= None;
inputSignal(256-1 downto 224) <= None;
inputSignal(224-1 downto 192) <= None;
inputSignal(192-1 downto 160) <= None;
inputSignal(160-1 downto 128) <= None;
inputSignal(128-1 downto 96) <= None;
inputSignal(96-1 downto 64) <= None;
inputSignal(64-1 downto 32) <= None;
inputSignal(32-1 downto 0) <= None;

PEEKDETECTOR_MAP_IN: process (inputSignal) is
begin
    for i in 0 to 100-1 loop
        input_sliced(i) <= inputSignal((32 * (i + 1))-1 downto (32 * i));
    end loop;
end process PEEKDETECTOR_MAP_IN;

PEEKDETECTOR_SUM_PROC: process (clk) is
    variable lookahead: natural;
    variable delta: natural;
    variable mn: integer;
    variable mx: integer;
    variable index: integer;
    variable mxpos: std_logic;
    variable maxTemp: integer;
    variable minTemp: integer;
    variable inc: integer;
    variable yValue: integer;
begin
    if rising_edge(clk) then
        if (reset = '0') then
            outputSignal2(0) <= to_unsigned(0, 32);
            outputSignal2(1) <= to_unsigned(0, 32);
            outputSignal2(2) <= to_unsigned(0, 32);
            outputSignal2(3) <= to_unsigned(0, 32);
            outputSignal2(4) <= to_unsigned(0, 32);
            outputSignal2(5) <= to_unsigned(0, 32);
            outputSignal2(6) <= to_unsigned(0, 32);
            outputSignal2(7) <= to_unsigned(0, 32);
            outputSignal2(8) <= to_unsigned(0, 32);
            outputSignal2(9) <= to_unsigned(0, 32);
            outputSignal2(10) <= to_unsigned(0, 32);
            outputSignal2(11) <= to_unsigned(0, 32);
            outputSignal2(12) <= to_unsigned(0, 32);
            outputSignal2(13) <= to_unsigned(0, 32);
            outputSignal2(14) <= to_unsigned(0, 32);
            outputSignal2(15) <= to_unsigned(0, 32);
            outputSignal2(16) <= to_unsigned(0, 32);
            outputSignal2(17) <= to_unsigned(0, 32);
            outputSignal2(18) <= to_unsigned(0, 32);
            outputSignal2(19) <= to_unsigned(0, 32);
            outputSignal2(20) <= to_unsigned(0, 32);
            outputSignal2(21) <= to_unsigned(0, 32);
            outputSignal2(22) <= to_unsigned(0, 32);
            outputSignal2(23) <= to_unsigned(0, 32);
            outputSignal2(24) <= to_unsigned(0, 32);
            outputSignal2(25) <= to_unsigned(0, 32);
            outputSignal2(26) <= to_unsigned(0, 32);
            outputSignal2(27) <= to_unsigned(0, 32);
            outputSignal2(28) <= to_unsigned(0, 32);
            outputSignal2(29) <= to_unsigned(0, 32);
            outputSignal2(30) <= to_unsigned(0, 32);
            outputSignal2(31) <= to_unsigned(0, 32);
            outputSignal2(32) <= to_unsigned(0, 32);
            outputSignal2(33) <= to_unsigned(0, 32);
            outputSignal2(34) <= to_unsigned(0, 32);
            outputSignal2(35) <= to_unsigned(0, 32);
            outputSignal2(36) <= to_unsigned(0, 32);
            outputSignal2(37) <= to_unsigned(0, 32);
            outputSignal2(38) <= to_unsigned(0, 32);
            outputSignal2(39) <= to_unsigned(0, 32);
            outputSignal2(40) <= to_unsigned(0, 32);
            outputSignal2(41) <= to_unsigned(0, 32);
            outputSignal2(42) <= to_unsigned(0, 32);
            outputSignal2(43) <= to_unsigned(0, 32);
            outputSignal2(44) <= to_unsigned(0, 32);
            outputSignal2(45) <= to_unsigned(0, 32);
            outputSignal2(46) <= to_unsigned(0, 32);
            outputSignal2(47) <= to_unsigned(0, 32);
            outputSignal2(48) <= to_unsigned(0, 32);
            outputSignal2(49) <= to_unsigned(0, 32);
            outputSignal2(50) <= to_unsigned(0, 32);
            outputSignal2(51) <= to_unsigned(0, 32);
            outputSignal2(52) <= to_unsigned(0, 32);
            outputSignal2(53) <= to_unsigned(0, 32);
            outputSignal2(54) <= to_unsigned(0, 32);
            outputSignal2(55) <= to_unsigned(0, 32);
            outputSignal2(56) <= to_unsigned(0, 32);
            outputSignal2(57) <= to_unsigned(0, 32);
            outputSignal2(58) <= to_unsigned(0, 32);
            outputSignal2(59) <= to_unsigned(0, 32);
            outputSignal2(60) <= to_unsigned(0, 32);
            outputSignal2(61) <= to_unsigned(0, 32);
            outputSignal2(62) <= to_unsigned(0, 32);
            outputSignal2(63) <= to_unsigned(0, 32);
            outputSignal2(64) <= to_unsigned(0, 32);
            outputSignal2(65) <= to_unsigned(0, 32);
            outputSignal2(66) <= to_unsigned(0, 32);
            outputSignal2(67) <= to_unsigned(0, 32);
            outputSignal2(68) <= to_unsigned(0, 32);
            outputSignal2(69) <= to_unsigned(0, 32);
            outputSignal2(70) <= to_unsigned(0, 32);
            outputSignal2(71) <= to_unsigned(0, 32);
            outputSignal2(72) <= to_unsigned(0, 32);
            outputSignal2(73) <= to_unsigned(0, 32);
            outputSignal2(74) <= to_unsigned(0, 32);
            outputSignal2(75) <= to_unsigned(0, 32);
            outputSignal2(76) <= to_unsigned(0, 32);
            outputSignal2(77) <= to_unsigned(0, 32);
            outputSignal2(78) <= to_unsigned(0, 32);
            outputSignal2(79) <= to_unsigned(0, 32);
            outputSignal2(80) <= to_unsigned(0, 32);
            outputSignal2(81) <= to_unsigned(0, 32);
            outputSignal2(82) <= to_unsigned(0, 32);
            outputSignal2(83) <= to_unsigned(0, 32);
            outputSignal2(84) <= to_unsigned(0, 32);
            outputSignal2(85) <= to_unsigned(0, 32);
            outputSignal2(86) <= to_unsigned(0, 32);
            outputSignal2(87) <= to_unsigned(0, 32);
            outputSignal2(88) <= to_unsigned(0, 32);
            outputSignal2(89) <= to_unsigned(0, 32);
            outputSignal2(90) <= to_unsigned(0, 32);
            outputSignal2(91) <= to_unsigned(0, 32);
            outputSignal2(92) <= to_unsigned(0, 32);
            outputSignal2(93) <= to_unsigned(0, 32);
            outputSignal2(94) <= to_unsigned(0, 32);
            outputSignal2(95) <= to_unsigned(0, 32);
            outputSignal2(96) <= to_unsigned(0, 32);
            outputSignal2(97) <= to_unsigned(0, 32);
            outputSignal2(98) <= to_unsigned(0, 32);
            outputSignal2(99) <= to_unsigned(0, 32);
        else
            lookahead := 10;
            delta := 0;
            mn := 0;
            mx := 0;
            index := 0;
            mxpos := '0';
            maxTemp := 0;
            minTemp := 0;
            inc := 0;
            for y in 0 to (100 - lookahead)-1 loop
                yValue := to_integer(input_sliced(y));
                if (yValue > mx) then
                    mx := yValue;
                    mxpos := stdl(index);
                end if;
                if (yValue < mn) then
                    mn := yValue;
                end if;
                for i in index to (index + lookahead)-1 loop
                    if (signed(resize(input_sliced(i), 33)) > maxTemp) then
                        maxTemp := to_integer(input_sliced(i));
                    end if;
                    if (signed(resize(input_sliced(i), 33)) < minTemp) then
                        minTemp := to_integer(input_sliced(i));
                    end if;
                end loop;
                if ((yValue < (mx - delta)) and (mx /= 0)) then
                    if (maxTemp < mx) then
                        outputSignal2(inc) <= to_unsigned(mxpos, 32);
                        inc := (inc + 1);
                        mx := 0;
                        mn := 0;
                    end if;
                end if;
                if ((yValue > (mn + delta)) and (mn /= 0)) then
                    if (minTemp > mn) then
                        mn := 0;
                        mx := 0;
                    end if;
                end if;
                index := (index + 1);
                minTemp := 6000;
                maxTemp := 0;
            end loop;
        end if;
    end if;
end process PEEKDETECTOR_SUM_PROC;


outputSignal <= out_c;

end architecture MyHDL;
