
import random
import matplotlib.pyplot as plt

def peakdetect(y_axis, x_axis=None, lookahead=500, delta=0):

    maxtab = []

    length = len(y_axis)
    baxis = []

    if x_axis is None:
        x_axis = range(length)

    mn, mx = 0, 0

    for index, y in enumerate(baxis[:-lookahead]):
        if y > mx:
            mx = y
            mxpos = index
        if y < mn:
            mn = y

        if y < mx - delta and mx != 0:
            if max(baxis[index:index + lookahead]) < mx:
                maxtab.append(mxpos)
                mx = 0
                mn = 0

        if y > mn + delta and mn != 0:
            if min(baxis[index:index + lookahead]) > mn:
                mn = 0
                mx = 0

    return maxtab, plt


rand = []
rand = random.sample(range(1, 100000), 10000)

maxtab,plt = peakdetect(rand, None, 10, 0)

print("maxtab")
for i in maxtab:
    print(i)

plt.show()