from myhdl import block, always_seq, Signal, intbv, enum, instances, ConcatSignal, always_comb


@block
def peekDetector(clk, reset, inputSignal, outputSignal, inputLookAhead=50, inputDelta=0, inputLength=5, outputLength=5,
                 dataSize=24):
    input_sliced = [Signal(intbv()[dataSize:]) for _ in range(inputLength)]
    outputSignal2 = [Signal(intbv()[dataSize:]) for _ in range(inputLength)]
    out_c = Signal(intbv()[dataSize * inputLength:])

    @always_comb
    def map_in():
        for i in range(inputLength):
            input_sliced[i].next = inputSignal[dataSize * (i + 1): dataSize * i]

    @always_seq(clk.posedge, reset=reset)
    def sum_proc():

        lookahead = inputLookAhead
        delta = inputDelta
        mn = 0
        mx = 0
        index = 0
        mxpos = 0
        maxTemp = 0
        minTemp = 0
        inc = 0
        for y in range(inputLength - lookahead):
            yValue = int(input_sliced[y])
            if yValue > mx:
                mx = int(yValue)
                mxpos = index
            if yValue < mn:
                mn = int(yValue)

            for i in range(index, index + lookahead):
                if input_sliced[i] > maxTemp:
                    maxTemp = int(input_sliced[i])
                if input_sliced[i] < minTemp:
                    minTemp = int(input_sliced[i])

            if yValue < mx - delta and mx != 0:
                if maxTemp < mx:
                    outputSignal2[inc].next = mxpos  # inputSignal[dataSize * (i + 1): dataSize * i]
                    inc += 1
                    mx = 0
                    mn = 0

            if yValue > mn + delta and mn != 0:
                if minTemp > mn:
                    mn = 0
                    mx = 0

            index += 1
            minTemp = 6000
            maxTemp = 0

    out_c = ConcatSignal(*outputSignal2)

    @always_comb
    def map_out():
        outputSignal.next = out_c

        # maxtab_tmp = [Signal(intbv(i)[dataSize:]) for i in outputSignal2]

    # outputSignal.next = ConcatSignal(*maxtab_tmp)

    # i+=1
    #  for i in range(outputLength):
    #    outputSignal.next = outputSignal2[dataSize * (i + 1): dataSize * i]

    return instances()
