from myhdl import block, instance, Signal, ResetSignal, StopSimulation, intbv, delay, ConcatSignal, instances
from clk_stim import clk_stim
from peakDetection import peekDetector
import random
import os


@block
def test_filter(vhdl_output_path=None):

    clk = Signal(bool(0))
    reset = ResetSignal(0, active=0, async=False)
    clk_gen = clk_stim(clk, period=10)

    inputLength = 100
    outputLength = 100
    dataSize = 32
    inputLookAhead = 10
    inputDelta = 0
    randArray = random.sample(range(1, 5000), 100)

    inputSignal = ConcatSignal(*[Signal(intbv(i)[dataSize:]) for i in randArray])
    outSignal = Signal(intbv(0)[dataSize*outputLength:])
    uut = peekDetector(clk, reset, inputSignal, outSignal, inputLookAhead, inputDelta, inputLength, outputLength, dataSize)

    @instance
    def reset_gen():
        reset.next = 0
        yield delay(10)
        yield clk.negedge
        reset.next = 1

    if vhdl_output_path is not None:
        uut.convert(hdl='VHDL', path=vhdl_output_path)

    return instances()


if __name__ == '__main__':
    trace_save_path = 'out/testbench/'
    vhdl_output_path = 'out/vhdl/'
    os.makedirs(os.path.dirname(trace_save_path), exist_ok=True)
    os.makedirs(os.path.dirname(vhdl_output_path), exist_ok=True)

    tb = test_filter(vhdl_output_path)
    tb.config_sim(trace=True, directory=trace_save_path, name='peakDet')
    tb.run_sim(200)
